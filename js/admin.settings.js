
/**
 * Conditionally show/hide reminder settings based on reminder days options.
 *
 * The email options settings are conditional on whether the signup days
 * before is configured to display. We use jQuery to hide the email options
 * settings when they're not relevant.
 */
Drupal.behaviors.signupShowFormEmailOptionsSetting = function () {
  $('div.signup-reminders-display-options input.form-radio').click(function () {
    // Simple part: Depending on the days before settings, hide/show
    // the collapsible fieldset setting.
    if (this.value == 1) {
      $('div.signup-reminders-display-dependent-options').show();
    }
    else {
      $('div.signup-reminders-display-dependent-options').hide();
    }
  });
};
