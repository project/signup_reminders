<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function signup_reminders_example_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function signup_reminders_example_node_info() {
  $items = array(
    'event' => array(
      'name' => t('Event'),
      'module' => 'features',
      'description' => t('A signup event'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_signup_config_defaults().
 */
function signup_reminders_example_signup_config_defaults() {
  $signup_configs = array();
  $signup_configs['confirmation_email'] = 'Hi %user_name,

You have signed up for the event:
%node_title
%node_start_time

--
To cancel this signup reminder: %cancel_signup_url
To edit your signup reminder information login or register then go to: %node_url';
  $signup_configs['reminder_days_before'] = '1';
  $signup_configs['reminder_email'] = 'Hi %user_name,

This is an event reminder for:
%node_title
%node_start_time

--
To cancel this signup reminder: %cancel_signup_url
To edit your signup reminder information login or register then go to: %node_url';
  $signup_configs['send_confirmation'] = '1';
  $signup_configs['send_reminder'] = '1';
  return $signup_configs;
}
